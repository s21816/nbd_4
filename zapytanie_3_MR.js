var mapFunction = function () {
    var key = this.job;
    var value = 1;
    emit(key, value);
}

var reduceFunction = function (key, value) {
    return Array.sum(value);
}

db.people.mapReduce(mapFunction, reduceFunction, { out: "mapreduce"})
printjson(db.mapreduce.find().toArray())
