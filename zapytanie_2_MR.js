
var mapFunction = function () {
    for (idx = 0; idx < this.credit.length; idx++) {
        var key = this.credit[idx].currency;
        var value = +this.credit[idx].balance;
        emit(key, value);
    }
}

var reduceFunction = function (key, value) {
    return Math.round(Array.sum(value) * 100) / 100;
}

db.people.mapReduce(mapFunction, reduceFunction, { out: "mapreduce"})
printjson(db.mapreduce.find({}).toArray())
