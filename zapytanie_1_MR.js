var mapFunction = function () {
    var key = this.sex;
    var value = { count: 1, weight: this.weight, height: this.height }
    emit(key, value);
}

var reduceFunction = function (key, objVals) {
    reducedVal = { count: 0, weight: 0, height: 0 }
    for (var idx = 0; idx < objVals.length; idx++) {
        reducedVal.count += objVals[idx].count;
        reducedVal.weight += +objVals[idx].weight;
        reducedVal.height += +objVals[idx].height;
    }
    return reducedVal;
}
var finalizeFunction = function (key, reducedVal) {
    reducedVal.weight = Math.round((reducedVal.weight / reducedVal.count) * 1000) / 1000;
    reducedVal.height = Math.round((reducedVal.height / reducedVal.count) * 1000) / 1000;
    return reducedVal;
};
db.people.mapReduce(mapFunction, reduceFunction, { out: "mapreduce", finalize: finalizeFunction })
printjson(db.mapreduce.find({}).toArray())

