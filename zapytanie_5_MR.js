var mapFunction = function () {
    for (idx = 0; idx < this.credit.length; idx++) {
        var key = this.credit[idx].currency;
        var value = { count: 1, balance: +this.credit[idx].balance };
        emit(key, value);
    }
}

var reduceFunction = function (key, objVals) {
    reducedVal = { count: 0, balance: 0 };
    for (idx = 0; idx < objVals.length; idx++) {
        reducedVal.count += objVals[idx].count;
        reducedVal.balance += objVals[idx].balance;
    }
    return reducedVal
}

var finalizeFunction = function (objkey, reducedVal) {
    reducedVal.balance = Math.round(reducedVal.balance * 100) / 100;
    reducedVal.avarage = Math.round((reducedVal.balance / reducedVal.count) * 100) / 100;
    return reducedVal;
};

db.people.mapReduce(mapFunction, reduceFunction, { query: { sex: "Female" ,nationality: "Poland" }, out: "mapreduce", finalize: finalizeFunction })
printjson(db.mapreduce.find().toArray())

