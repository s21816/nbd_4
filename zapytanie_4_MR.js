var mapFunction = function () {
    var key = this.nationality;
    var bmi = Math.round((+this.weight * 1000000 / (Math.pow(+this.height,2))) ) / 100
    var value = { count: 1, minBMI: bmi, maxBMI: bmi, avarageBMI: bmi }
    emit(key, value);
}

var reduceFunction = function (key, objVals) {
    reducedVal = { count: 0, minBMI: 0, maxBMI: 0, avarageBMI: 0 }
    for (var idx = 0; idx < objVals.length; idx++) {
        reducedVal.count += objVals[idx].count;
        if (reducedVal.minBMI == 0) {
            reducedVal.minBMI = objVals[idx].minBMI
        }
        if (objVals[idx].minBMI < reducedVal.minBMI) {
            reducedVal.minBMI = objVals[idx].minBMI
        }
        if (objVals[idx].maxBMI > reducedVal.maxBMI) {
            reducedVal.maxBMI = objVals[idx].maxBMI
        }
        reducedVal.avarageBMI += objVals[idx].avarageBMI;
    }
    return reducedVal;
}
var finalizeFunction = function (key, reducedVal) {
    reducedVal.avarageBMI = Math.round((reducedVal.avarageBMI / reducedVal.count) * 100) / 100;
    return reducedVal;
};

db.people.mapReduce(mapFunction, reduceFunction, { out: "mapreduce", finalize: finalizeFunction })
printjson(db.mapreduce.find({}).toArray())
